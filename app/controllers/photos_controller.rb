class PhotosController < ApplicationController
  def index
    @photos = Photo.with_attached_file.all.order(:id)
  end

  def upload_to_google
    @photo = Photo.with_attached_file.find(params[:id])

    upload_token = upload_image(@photo)
    logger.debug("upload token: #{upload_token}")
    result = create_media_item(upload_token)
    logger.debug("batchCreate result: #{result}")

    redirect_to root_path, notice: 'Googleフォトへのアップロードが完了しました。'
  end

  private

  def upload_image(photo)
    uri = ::URI.parse('https://photoslibrary.googleapis.com/v1/uploads')

    req = ::Net::HTTP::Post.new(uri)
    req['Authorization'] = "Bearer #{session[:access_token]}"
    req['Content-Type'] = 'application/octet-stream'
    req['X-Goog-Upload-File-Name'] = photo.file.blob.filename
    req.body = photo.file.blob.download

    res = ::Net::HTTP.start(uri.host, uri.port, use_ssl: true) do |http|
      http.request(req)
    end
    res.is_a?(::Net::HTTPSuccess) ? res.body : res.error!
  end

  def create_media_item(upload_token)
    uri = ::URI.parse('https://photoslibrary.googleapis.com/v1/mediaItems:batchCreate')

    req = ::Net::HTTP::Post.new(uri)
    req['Authorization'] = "Bearer #{session[:access_token]}"
    req['Content-Type'] = 'application/json'
    req.body = {
      newMediaItems: [
        {
          description: "Google Photos API test",
          simpleMediaItem: {
            uploadToken: upload_token
          }
        }
      ]
    }.to_json

    res = ::Net::HTTP.start(uri.host, uri.port, use_ssl: true) do |http|
      http.request(req)
    end
    res.is_a?(::Net::HTTPSuccess) ? res.body : res.error!
  end
end
