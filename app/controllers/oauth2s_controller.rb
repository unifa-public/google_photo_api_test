class Oauth2sController < ApplicationController
  def new
    uri = ::URI.parse('https://accounts.google.com/o/oauth2/v2/auth')
    params = {
      client_id: Rails.application.credentials.google[:api][:client_id],
      scope: 'https://www.googleapis.com/auth/photoslibrary.appendonly',
      redirect_uri: callback_oauth2_url,
      response_type: 'code',
    }
    uri.query = ::URI.encode_www_form(params)

    redirect_to uri.to_s
  end

  def callback
    if params[:code].blank?
      redirect_to root_url, alert: 'Google連携がキャンセルされました。' and return
    end

    uri = ::URI.parse('https://www.googleapis.com/oauth2/v4/token')
    req_params = {
      code: params[:code],
      client_id: Rails.application.credentials.google[:api][:client_id],
      client_secret: Rails.application.credentials.google[:api][:client_secret],
      grant_type: 'authorization_code',
      redirect_uri: callback_oauth2_url,
    }
    res = ::Net::HTTP.post_form(uri, req_params)

    result = ::JSON.parse(res.body)
    session[:access_token] = result['access_token']

    redirect_to root_url, notice: 'Google連携に成功しました。'
  end
end
