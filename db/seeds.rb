# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Rails.root.join('db/images').children.each.with_index do |file, i|
  photo = Photo.create!(title: "test_#{i}")
  photo.file.attach(io: File.open(file), filename: file.basename, content_type: 'image/jpeg')
end
