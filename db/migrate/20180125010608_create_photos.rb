class CreatePhotos < ActiveRecord::Migration[5.2]
  def change
    create_table :photos do |t|
      t.string :title, limit: 255, null: false

      t.timestamps
    end
  end
end
