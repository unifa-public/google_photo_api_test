# Google Photos APIテスト用プロジェクト

## 環境

* Ruby 2.5.1
* Postgresql

## 準備

はじめにGoogle Developer Consoleから以下の作業を実施

* プロジェクトの作成
* Google Photos APIの有効化
* OAuthクライアントIDを登録(client_idとclient_secretが発行されるのでメモ)
  - 承認済みのJavaScript生成元は `http://localhost:3000`
  - 承認済みのリダイレクトURIは `http://localhost:3000/oauth2/callback`

gemとDB周り、 `config/database.yml` は自身の環境に合わせて修正してください。

    $ bundle install
    $ rails db:create
    $ rails db:migrate
    $ rails db:seed

OAuth周りの設定
    
    $ rm config/credentials.yml.enc
    $ EDITOR=vim rails credentials:edit

以下を追加、client_id と client_secretは自身のものを設定
```yml
google:
  api:
    client_id: "your client_id"
    client_secret: "your client_secret"
```

## 起動方法

    $ rails s
    $ open http://localhost:3000
