Rails.application.routes.draw do
  resources :photos, only: [:index] do
    member do
      post :upload_to_google
    end
  end

  resource :oauth2, only: [:new] do
    get :callback
  end

  root to: 'photos#index'
end
